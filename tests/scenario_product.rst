===================
Production Scenario
===================

""""
- Clean input lots
- Create internal shipment
""""

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import config, Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> today = datetime.date.today()
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> yesterday = today - relativedelta(days=1)
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, create_chart, get_accounts, create_tax

Install production Module::

    >>> config = activate_modules(['electrans_product_obsolescence'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create taxes::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()
    >>> tax2 = create_tax(Decimal('.9'))
    >>> tax2.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Bienes")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.supplier_taxes.append(tax2)
    >>> account_category.save()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> templatedeprecated = ProductTemplate()
    >>> templatedeprecated.name = 'productdeprecated'
    >>> templatedeprecated.default_uom = unit
    >>> templatedeprecated.type = 'goods'
    >>> templatedeprecated.list_price = Decimal(30)
    >>> templatedeprecated.cost_price = Decimal(20)
    >>> templatedeprecated.account_category = account_category
    >>> templatedeprecated.deprecated = True
    >>> templatedeprecated.discontinued_by_engineering = True
    >>> templatedeprecated.save()
    >>> templatenondeprecated = ProductTemplate()
    >>> templatenondeprecated.name = 'productnondeprecated'
    >>> templatenondeprecated.deprecated = False
    >>> templatenondeprecated.default_uom = unit
    >>> templatenondeprecated.type = 'goods'
    >>> templatenondeprecated.list_price = Decimal(30)
    >>> templatenondeprecated.cost_price = Decimal(20)
    >>> templatenondeprecated.account_category = account_category
    >>> templatenondeprecated.save()
    >>> product.template = templatedeprecated
    >>> product.producible = True
    >>> product.save()
    >>> product.deprecated = True
    >>> product.discontinued_by_engineering = True
    >>> product.save()

Test deprecated product rec_name::

    >>> "!" in templatedeprecated.rec_name
    True
    >>> "!" in templatenondeprecated.rec_name
    False
    >>> "!" in product.rec_name
    True
