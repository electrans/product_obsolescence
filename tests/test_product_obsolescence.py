#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_setup
from trytond.tests.test_tryton import doctest_checker
from trytond.pool import Pool
from trytond.modules.electrans_tools.tests.test_tools import (
    create_template)


class ProductObsolescenceTestCase(ModuleTestCase):
    'Test module'
    module = 'electrans_product_obsolescence'
    
    @with_transaction()
    def test_check_template_products(self):
        """
        Test to check if template products are prevented to be used
        in productions, sales and purchases
        """
        pool = Pool()
        Template = pool.get('product.template')
        ProductUom = pool.get('product.uom')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        template = create_template('template')
        template.deprecated = True
        template.prevent_in_production = False
        template.save()
        Template.check_deprecated_products()
        self.assertTrue(template.prevent_in_production)
        self.assertFalse(template.producible)
        self.assertFalse(template.purchasable)
        template.prevent_in_production = True
        template.salable = True
        template.sale_uom = unit
        template.save()
        Template.check_deprecated_products()
        self.assertTrue(template.prevent_in_production)
        self.assertFalse(template.salable)
        self.assertFalse(template.producible)
        self.assertFalse(template.purchasable)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(ProductObsolescenceTestCase))
    suite.addTests(doctest.DocFileSuite('scenario_product.rst',
                                        setUp=doctest_setup, tearDown=doctest_teardown,
                                        encoding='utf-8', checker=doctest_checker,
                                        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite

