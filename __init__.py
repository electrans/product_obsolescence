# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import production


def register():
    Pool.register(
        product.Cron,
        product.ObsolescenceReason,
        product.Template,
        product.Product,
        production.Production,
        production.BOM,
        module='electrans_product_obsolescence', type_='model')
