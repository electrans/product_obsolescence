# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.i18n import gettext
from trytond.exceptions import UserError

__all__ = ['Production', 'BOM']


class Production(metaclass=PoolMeta):
    __name__ = 'production'

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for productions, values in zip(actions, actions):
            state = values.get('state')
            if state:
                for production in productions:
                    if production.state == 'request' and state == 'draft' or production.state == 'draft' \
                            and state == 'waiting' or production.state == 'waiting' and state == 'assigned' \
                            or production.state == 'assigned' and state == 'running' or \
                            production.state == 'running' and state == 'done':
                        for move in production.inputs:
                            if move.product and move.product.template and move.product.template.prevent_in_production:
                                raise UserError(gettext('electrans_product_obsolescence.invalid_product',
                                                        product=move.product.rec_name))
        return super(Production, cls).write(*args)


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'

    @classmethod
    def __setup__(cls):
        super(BOM, cls).__setup__()
        cls.inputs.target_search = [('product.template.prevent_in_production', '=', False)]