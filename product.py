# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not
import logging
from trytond.transaction import Transaction

logger = logging.getLogger(__name__)
__all__ = ['Template', 'ObsolescenceReason']


class ObsolescenceReason(ModelSQL, ModelView, metaclass=PoolMeta):
    'Product Obsolescence Reason'
    __name__ = 'product.obsolescence.reason'
    _rec_name = 'obsolescence_reason'

    obsolescence_reason = fields.Char('Reason')

    @classmethod
    def copy(cls, products, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('obsolescence_reason', None)
        return super(ObsolescenceReason, cls).copy(products, default=default)


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    prevent_in_production = fields.Boolean('Prevent in production input')
    prevent_in_new_designs = fields.Boolean('Prevent in new designs',
                                            help='Hidden in Altium and cannot be used in LdM.')
    discontinued_by_purchases = fields.Boolean('Discontinued by purchases', states={
        'required': (Bool(Eval('deprecated'))) & (Not(Bool(Eval('discontinued_by_engineering'))))})
    discontinued_by_engineering = fields.Boolean('Discontinued by engineering', states={
        'required': (Bool(Eval('deprecated'))) & (Not(Bool(Eval('discontinued_by_purchases'))))})
    discontinued_reason = fields.Many2One('product.obsolescence.reason', 'Reason')
    substitute = fields.Many2One('product.template', 'Substitute')
    discontinued_exception_date = fields.Date('Exception Date',
                                              help='Date until the product can be used in exception case.')
    deprecated = fields.Boolean('Deprecated')
    deprecated_symbol = fields.Function(fields.Char('Deprecated'), 'get_deprecated_symbol')
    deprecated_bom_inputs = fields.Function(fields.Boolean(
        'Deprecated BOM inputs'), 'get_deprecated_bom_inputs')
    obsolescence_observations = fields.Text('Observations', help='Observations for the product obsolescence.')

    def get_deprecated_bom_inputs(self, name):
        for bom in self.boms:
            if bom.active_version:
                for line in bom.active_version.inputs:
                    if line.product.deprecated and line.product.prevent_in_production:
                        return True

    def get_rec_name(self, name):
        rec_name = super(Template, self).get_rec_name(name)
        symbol = self.deprecated_symbol
        if symbol:
            rec_name = symbol + " " + rec_name
        return rec_name

    def get_deprecated_symbol(self, name):
        return "!" if self.deprecated else "?" if self.deprecated_bom_inputs else ""

    @classmethod
    def copy(cls, products, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('prevent_in_production', False)
        default.setdefault('prevent_in_new_designs', False)
        default.setdefault('discontinued_by_purchases', False)
        default.setdefault('discontinued_by_engineering', False)
        default.setdefault('discontinued_reason', None)
        default.setdefault('substitute', None)
        default.setdefault('discontinued_exception_date', None)
        default.setdefault('deprecated', False)
        default.setdefault('deprecated_symbol', None)
        default.setdefault('deprecated_bom_inputs', None)
        default.setdefault('obsolescence_observations', None)
        return super(Template, cls).copy(products, default)

    @classmethod
    def view_attributes(cls):
        return super(Template, cls).view_attributes() + [
            ('/form/notebook/page[@name="deprecated"]', 'states', {
                'invisible': Not(Bool(Eval('deprecated')))
            }
             )]

    @fields.depends('deprecated', 'prevent_in_production', 'prevent_in_new_designs', 'discontinued_by_purchases',
                    'discontinued_by_engineering', 'discontinued_reason', 'substitute', 'discontinued_exception_date')
    def on_change_deprecated(self):
        if not self.deprecated:
            self.prevent_in_production = False
            self.prevent_in_new_designs = False
            self.discontinued_by_purchases = False
            self.discontinued_by_engineering = False
            self.discontinued_reason = None
            self.substitute = None
            self.discontinued_exception_date = None

    @classmethod
    def check_deprecated_products(cls):
        """
        Method to check deprecated products without stock and prevent them to be used in productions, sales and purchases
        """
        logger.info('Checking deprecated products')
        pool = Pool()
        Template = pool.get('product.template')
        templates_updated = []
        warehouses = pool.get('stock.location').search(['type', '=', 'warehouse'])
        # Take only StorageLocation and PalletZone from all warehouses
        ware_internal = []
        for w in warehouses:
            if w.pallet_zone:
                ware_internal.append(w.pallet_zone)
            if w.storage_location:
                ware_internal.append(w.storage_location)
            if w.name == 'Almacén Electrans':
                # Get warehouse with code TALLER only in this warehouse
                taller = w.search([('code', '=', 'TALLER')])
                if taller:
                    ware_internal.append(taller[0])
        # get products in "Zones" selected
        with Transaction().set_context(locations=[w.id for w in ware_internal], with_childs=True):
            templates = Template.search([
                'OR', [
                    ('deprecated', '=', True),
                    ('prevent_in_production', '=', False),
                ], [
                    ('deprecated', '=', True),
                    ('prevent_in_production', '=', True),
                    ['OR',
                     ('salable', '=', True),
                     ('purchasable', '=', True),
                     ('producible', '=', True),
                     ],
                ]
            ])
        for template in templates:
            # prevent_in_production if qty and forecast_qty are 0.0
            if not (template.quantity or template.forecast_quantity):
                templates_updated.append(template)
        if templates_updated:
            # Separate salable products and the others
            # cause not sending unnecessary emails
            salable_templates_updated = [t for t in
                                         templates_updated if t.salable]
            # Convert lists to a set to allow to subtract between them
            salable_templates_updated = set(salable_templates_updated)
            templates_updated = set(templates_updated)
            not_salable_templates_updated = list(
                    templates_updated - salable_templates_updated)

            Template.write(list(salable_templates_updated), {
                'prevent_in_production': True,
                'purchasable': False,
                'salable': False,
                'producible': False,
            })
            Template.write(not_salable_templates_updated, {
                'prevent_in_production': True,
                'purchasable': False,
                'producible': False,
            })
        logger.info('Cron for prevent products deprecated in productions: products updated: %s' %
                    templates_updated)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    def get_rec_name(self, name):
        rec_name = super(Product, self).get_rec_name(name)
        symbol = self.deprecated_symbol
        if symbol:
            rec_name = symbol + " " + rec_name
        return rec_name

    def get_substitute(self):
        """
        Get id from the latest substitute for this product
        """
        if self.template.prevent_in_production and self.template.substitute:
            res = self.template.substitute.products[0].get_substitute()
        else:
            res = self.id
        return res


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.method.selection.append(
            ('product.template|check_deprecated_products', "Check deprecated product for prevent in Productions"),)
